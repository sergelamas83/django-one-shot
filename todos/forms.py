from django import forms
from .models import TodoItem, TodoList


class TodoItemForm(forms.ModelForm):
    class Meta:
        model = TodoItem
        fields = ["task", "due_date", "is_completed", "list"]

    is_completed = forms.BooleanField(
        required=False
    )


class TodoListForm(forms.ModelForm):
    class Meta:
        model = TodoList
        fields = ["name"]
