from django.shortcuts import render, get_object_or_404, redirect
from .models import TodoList
from .forms import TodoItemForm, TodoListForm, TodoItem


def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    todo_items = todo_list.items.all()

    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todo_item = form.save(commit=False)
            todo_item.list_name = todo_list
            todo_item.save()
            return redirect("todo_list_detail", id=list.id)

    else:
        form = TodoItemForm()

    context = {
        "todo_list": todo_list,
        "todo_items": todo_items,
        "form": form,
    }

    return render(request, "todos/todo_list_detail.html", context)


def todo_list_list(request):
    todo_lists = TodoList.objects.all()
    context = {"todo_lists": todo_lists}

    return render(request, "todos/todo_list_list.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list = form.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoListForm()

    context = {
        "form": form,
    }
    return render(request, "todos/todo_list_create.html", context)


def todo_list_update(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoListForm(instance=todo_list)

    context = {
        "form": form,
    }
    return render(request, "todos/todo_list_update.html", context)


def todo_list_delete(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect("todo_list_list")

    context = {
        "todo_list": todo_list,
    }
    return render(request, "todos/todo_list_delete.html", context)


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)

    else:
        form = TodoItemForm()

    context = {
        "form": form,
    }
    return render(request, "todos/todo_item_create.html", context)


def todo_item_update(request, id):
    todo_item = get_object_or_404(TodoItem, id=id)

    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_item)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)

    else:
        form = TodoItemForm(instance=todo_item)

    context = {
        "form": form,
        "todo_item": todo_item,
    }
    return render(request, "todos/todo_item_update.html", context)
